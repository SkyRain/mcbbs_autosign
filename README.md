# Mcbbs_AutoSign_Python3 #

This is an auto tool for [mcbbs][1] users to sign.<br>
Open source with [GNU GPLv3][2]

## How to use
First, clone this project into your computer,execute:
```bash
# For Linux and Windows
git clone https://gitlab.com/SkyRain/mcbbs_autosign.git ./ 
```
Then,`cd` to the project dir and edit file `main.py`,You should edit these settings:
 - The `cookie_path` var.Change it to your cookie-save-path.
 - The `proxy` if you want to use proxies

Then,open the chrome window and go to mcbbs.Press `F12` to open the Chrome Dev Tools Window.<br>
Click the tab 'Network' and press `F5` to reflush the page. <br>
When it finish flushing, Click the first request like `forum.php` to see data.<br>
Then copy the request item `Cookies: `'s data like:
```
a=a;bbbbb=b;iewjdoe=dwdw;ddnwqduw=%20%20%20
```
(something strange......)<br>
Then create a file where you typed into the setting `cookie_path`,and paste the cookies into it.<br>
OK! Now you can execute command `python3 main.py` to run it! Enjoy! :)<br>

## Copyleft
Write by SkyRain.<br>
Thanks to KasuganoSora.<br>
Open source with [GPLv3][2]<br>
![GPLv3](http://www.gnu.org/graphics/gplv3-with-text-136x68.png "Open source with GNU GPLv3")

[1]: http://www.mcbbs.net
[2]: http://www.gnu.org/licenses/gpl-3.0.html