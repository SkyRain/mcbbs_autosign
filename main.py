#!/usr/bin/env python3

import requests as req
# from requests.cookies import RequestsCookieJar
import logging
import logging.handlers
import datetime
import time
import random
import threading

# Tools version
VERSION = '0.1-dev'

# Sign Texts
sign_texts = [ 
    '快乐咸鱼每一天~', 
	'我来签到啦~', 
	'祝 MCBBS 越来越好', 
	'滑稽，签到签到', 
	'我什么也不说，这是坠吼的', 
	'这个签到有点意思', 
	'签到真棒wwwww', 
	'签到有积分真好' 
] # Moe......

## Sign Proxy
# proxy = {'http': 'misakacloud.net:80', 'https': 'misakacloud.net:443'}
proxy = None

## Cookie file path
# Default use UTF-8
cookie_path = './cookie-t.txt'
cookie_cache = '' # DONOT CHANGE IT!!!

## File encoding
# Default use UTF-8
file_encoding = 'UTF-8'

## Python logging setting

logger = logging.getLogger('mcbbs_sign_logger')
logger.setLevel(logging.DEBUG)
'''
rf_handler = logging.handlers.TimedRotatingFileHandler('run.log', when='midnight', interval=1, backupCount=7, atTime=datetime.time(0, 0, 0, 0))
rf_handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(message)s"))

f_handler = logging.FileHandler('error.log')
f_handler.setLevel(logging.DEBUG) # Change it when release!
f_handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(filename)s[:%(lineno)d] - %(message)s"))
'''
debug_handler = logging.FileHandler('debug.log')
debug_handler.setLevel(logging.DEBUG)
debug_handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(filename)s[:%(lineno)d] - %(message)s"))
logger.addHandler(debug_handler)

# logger.addHandler(rf_handler)
# logger.addHandler(f_handler)

# ------ Run ------

# HTTP Get util
def util_get(url, cookie={}):
    if proxy: # Proxy set
        return req.get(url, proxies=proxy, verify=False, cookies=cookie)
    else: # Proxy not set
        return req.get(url, verify=False, cookies=cookie)
    pass

# HTTP Post util
def util_post(url, data={}, cookie={}, referer='https://misakacloud.net'):
    if proxy: # Proxy set
        return req.post(url, proxies=proxy, verify=False, cookies=cookie, headers={'referer': referer})
    else: # Proxy not set
        return req.post(url, verify=False, cookies=cookie, headers={'referer': referer})
    pass

# Stay for next day
# Default sleep one day
def util_stay(sec = 60 * 60 * 24):
    time.sleep(sec)
    pass

# Cookie file parse util
def util_cookie_parse(fileobject):
    if type(fileobject) == str: # It is a path str
        global file_encoding
        fileobject = open(file=fileobject, mode='r', encoding=file_encoding)
        pass
    cookie_data = fileobject.read() # Read file
    cookie_data = cookie_data.split(';') # Split it with ';'
    logger.debug('Cookie Data: %s' % repr(cookie_data))
    result = {}
    for cookie_item in cookie_data: # Parse them
        # cookie_item: string like 'misakacloud=%20%20%20&nbsp;666'
        v1 = cookie_item.split('=') # Split it with '='
        result[ v1[0] ] = v1[-1] # add it to result
        v1 = None # C++ !!!!!
        pass # end
    logger.debug('Parsed cookie-jar: %s' % result)
    fileobject.close() # Close stream
    return result # Visual Studio Code is toooooooooo low!!!

# Random text
def random_text(textlist=sign_texts):
    # Just a function...
    if len(textlist) == 0: # List is null
        return '签到个鬼哟'
    elif len(textlist) == 1: # List like a string now
        return textlist[0]
    random_num = random.randint(0, len(textlist)-1) # Build random list pointer
    return textlist[random_num] # Get random list item

# Main function
def compute():
    # MSKCloud supported
    # SkyRain wrote
    # Open source by GNU GPLv3
    #
    global logger
    global VERSION
    global cookie_path
    global cookie_cache
    logger.info('MCBBS Auto Sign Tools Version %s' % VERSION)
    logger.info('Powered by MSKCloud, SkyRain')
    logger.info('Open source by GNU GPLv3')
    print('Bilibiling......')
    logger.info('\n')
    cookie_jar = util_cookie_parse(cookie_path) # Parse cookie
    data = util_get('http://www.mcbbs.net/forum.php', cookie=cookie_jar) # Get
    logger.debug('HTTP Get success, status code %i'% data.status_code)
    substr = data.text.split('<input type="hidden" name="formhash" value="') # Cut the result
    logger.debug('Html cut success, length= %i'% len(substr))
    ## Note:
    #  Use the last found str part.
    # Balabalabala......
    fromhash = substr[-1][0:8] # Fromhash like 6666ccff
    logger.debug('Hash got: %s' % fromhash)
    # Start to sign
    random_sign_text = random_text()
    print("啦啦啦啦啦啦，开始签到")
    logger.info('Done.Start to sign.')
    logger.info('Random sign text: %s'% random_sign_text)
    print(random_sign_text)
    post_data = {
        'fromhash': fromhash,
        'signsubmit': 'yes',
        'handlekey': 'signin',
        'emotid': '1',
        'referer': 'http://www.mcbbs.net/plugin.php?id=dc_signin',
        'content': random_sign_text
    } # Build Post Data
    print('少女祈祷中...')
    result = util_post(url='http://www.mcbbs.net/plugin.php?id=dc_signin:sign&inajax=1', data=post_data, cookie=cookie_jar)
    logger.debug('POST Success, status_code: %i' % result.status_code)
    print('Done.\nChecking if sign successd')
    ## Note
    # Check cookie
    logger.debug('Server returned cookies:')
    logger.debug(repr(result.cookies))
    ## Note
    # Check response html
    # Use str.find()
    point = result.text.find('成功')
    if not point == -1: # Sign success
        message = result.text[point-250:point+250] # Message about 500 letters
        print('签到成功！前后信息：')
        print(message)
        logger.info('Message: \n%s' % message)
        ## Note
        # Save cookie to cookie cache
        cookie_cache = result.cookies.get_dict()
        return 'A'
    elif not result.text.find('已经') == -1: # It has been signed by others
        point = result.text.find('已经')
        print('已经手工签到啦！')
        logger.warning('It has been signed by others')
        logger.warning(result.text[point-250:point+250])
        ## Note
        # Save cookie to cookie cache
        cookie_cache = result.cookies.get_dict()
        return 'B'
    else: # Error
        print('签到失败！')
        logger.error('Error when try signing.Html:')
        logger.error(result.text)
        return 'C'


# Sign Thread class
class SignThread(threading.Thread):
    def __init__(self, tid):
        threading.Thread.__init__(self)
        self.tid = tid
        self.returnID = 'Daze~'
        pass
    def run(self):
        logger.info('Thread %i ready to run' % self.tid)
        self.returnID = compute()
        pass
    def getID(self):
        return self.returnID
    pass

### Main Function
# Powered by MSKCloud
# Daze~
if __name__ == "__main__":
    # Main
    print('Daze~')
    is_exit = False
    threadID = 0
    while not is_exit:
        # Start thread
        thread = SignThread(threadID)
        logger.info('Run thread %i'% threadID)
        thread.start()
        thread.join()
        result = thread.getID()
        print('%s' % result)
        if result == 'A':
            # Developing...
            print('Daze~')
            is_exit = True
            pass
        elif result == 'B':
            # Do Nothing
            pass
        elif result == 'C':
            print('Main Error Happend.')
            print('Please connect with the admin.')
            print('Continue to run.')
            pass
        else:
            is_exit = True
            pass
        threadID = threadID + 1
        pass
    pass